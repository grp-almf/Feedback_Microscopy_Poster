# Feedback Microscopy at the Advanced Light Microscopy at EMBL

## Aim

This repository is the landing page for the QR code and short link for our
Feedback microscopy poster at the EMBL ALMF. The repository holds a pdf version
of the poster as well as links to software downloads, documentation and papers.

<img src="./Images/feedback_microscopy_poster.png" width=830>


## Software Resources

### Cellprofiler Modules for Leica (ALMF):

<img src="./Images/cellprofiler.png" width=400>

**Author:** _Volker Hilsenstein_

**Summary:**
These plugin modules for CellProfiler enable you to quickly set up feedback microscopy experiments with Leica microscopes that have the Matrix Screener (HCS-A) software module. Installation is simple: just unzip the files and set your CellProfiler plugin path.

**Code:**
[This github page provides a tutorial andthe software download](https://github.com/VolkerH/MatrixScreenerCellprofiler/wiki)

### Cecoglink Tool for Feedback Microscopy using supervised learning (ALMF)

<img src="./Images/CecogLink.png" width=400>

**Author:** _Volker Hilsentein_

**Summary:**
This tool provides "glue code" between  [CellCognition](https://cellcognition-project.org/) and the Leica Matrix Screener. Cellcognition features basic object segmentation and powerful feature extraction and SVM-classification code together with an intuitive annotation interface.

Train a CellCognition classifier to recognize phenotypes by annotating examples of different phenotypes. Once you have trained the classifier in CellCognition you can use Cecoglink tool to start analysis of prescan images from the Leica and then trigger hi-resolution scans on the Leica.


**Code:**
[CecogLinkTool on EMBL Github - Code and Tutorial](https://git.embl.de/grp-almf/cecoglink)

### MyPic tool for interfacing with ZEN Black (Ellenberg Lab)

**Author:** _Antonio Politi_

**Summary:** Code to interface various image analysis scripts to the ZEN Black macro (for LSM 780/880 confocal microscopes)

**Code:**
[MyPic on EMBL GitLab](https://git.embl.de/politi/mypic
) 

**Publication:**
[Quantitative mapping of fluorescently tagged cellular proteins using FCS-calibrated four dimensional imaging](https://doi.org/10.1101/188862
)

## Further References

Tischer, C., Hilsenstein, V., Hanson, K., & Pepperkok, R. (2014). *Adaptive fluorescence microscopy by online feedback image analysis*. Methods in Cell Biology, 123, 489-503. [DOI: 10.1016/B978-0-12-420138-5.00026-4](http://dx.doi.org/10.1016/B978-0-12-420138-5.00026-4)

Altmetric: 5Citations: 60More detail
Brief Communication
Conrad, C., Wünsche, A., Heng Tan, T., Bulkescher, J., Sieckmann, F., Verissimo, F., Edelstein, A., Walter, T.,  Liebel, U.,  Pepperkok.R, Ellenberg.J
*Micropilot: automation of fluorescence microscopy–based imaging for systems biology, 
* [Nature Methods, 2011](https://www.nature.com/articles/nmeth.1558)